unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqlite3conn, sqldb, db, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DBGrids, ComCtrls, Buttons, ExtCtrls;

type

  { TFormMain }

  TFormMain = class(TForm)
    BtnAddRecord: TBitBtn;
    BtnConnect: TBitBtn;
    BtnDeleteRecord: TBitBtn;
    BtnRefresh: TBitBtn;
    BtnClose: TButton;
    BtnSave: TBitBtn;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    ImageList1: TImageList;
    Panel1: TPanel;
    Panel2: TPanel;
    SpeedButton1: TSpeedButton;
    Splitter2: TSplitter;
    SQLConnector1: TSQLConnector;
    SQLQueryList: TSQLQuery;
    SQLQueryTable1: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    StatusBar1: TStatusBar;
    TreeView1: TTreeView;
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnConnectClick(Sender: TObject);
    procedure BtnRefreshClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnAddRecordClick(Sender: TObject);
    procedure BtnDeleteRecordClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TreeView1SelectionChanged(Sender: TObject);
  private
    RootNode: TTreeNode;
    procedure MyException(Sender : TObject; E : Exception);
  public

  end;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

{ TFormMain }

procedure TFormMain.MyException(Sender: TObject; E: Exception);
begin
  MessageDlg('Ошибка', 'Возникла какая-то ошибка: ' + E.Message,
    mtError, [mbOK],0
  );
end;

procedure TFormMain.BtnRefreshClick(Sender: TObject);
var
  TmpNode: TTreeNode;
begin
  DBGrid1.Clear;
  SQLQueryTable1.Close;

  TmpNode := TreeView1.Selected;

  if (TmpNode = RootNode) or (TmpNode = nil)
  then
    Exit;

  try
    SQLQueryTable1.SQL.Text := 'SELECT * FROM ' + TmpNode.Text + ';';
    SQLTransaction1.Active := True;
    SQLQueryTable1.Open;
    StatusBar1.SimpleText := 'Успешное обновление данных по таблице: ' +
      TmpNode.Text;
  except
    on E: Exception do
    begin
      MessageDlg('Ошибка при обновлении', 'Возникла какая-то ошибка: ' +
        E.Message,
        mtError, [mbOK],0
      );
      StatusBar1.SimpleText := 'Ошибка обновления данных по таблице: ' +
        TmpNode.Text;
    end;
  end;
end;

procedure TFormMain.BtnConnectClick(Sender: TObject);
var
  TableName: String;
begin
  if BtnConnect.Caption = 'Подключиться'
  then
  begin
    try
      SQLConnector1.Connected := True;
      SQLTransaction1.Active := True;
      SQLQueryList.Open;
    except
      on E: Exception do
      begin
        MessageDlg('Ошибка при подключении', 'Возникла какая-то ошибка: ' +
          E.Message,
          mtError, [mbOK],0
        );

        StatusBar1.SimpleText :=
          'Ошибка подключения к базе sqlite_data.sqlite3';
        Exit;
      end;
    end;

    BtnConnect.Caption := 'Отключиться';
    BtnConnect.ImageIndex := 1;

    try
      SQLQueryList.First;

      while SQLQueryList.EOF = false do
      begin
        TableName := SQLQueryList.Fields[0].AsString;

        if Pos('sqlite_', TableName) = 1
        then
        begin
          SQLQueryList.Next;
          continue;
        end;

        TreeView1.Items.AddChild(RootNode, TableName);

        SQLQueryList.Next;
      end;

      RootNode.Expand(False);
    finally
      SQLQueryList.Close;
    end;

    StatusBar1.SimpleText := 'Успешное подключение к базе sqlite_data.sqlite3';
  end
  else
  begin
    TreeView1.Select([RootNode]);
    BtnConnect.Caption := 'Подключиться';
    BtnConnect.ImageIndex := 0;

    SQLQueryList.Close;
    SQLQueryTable1.Close;
    SQLTransaction1.Active := False;
    SQLConnector1.Connected := False;

    BtnAddRecord.Enabled := False;
    BtnDeleteRecord.Enabled := False;
    BtnRefresh.Enabled := False;
    BtnSave.Enabled := False;

    RootNode.DeleteChildren;
    StatusBar1.SimpleText := 'Соединение с базой sqlite_data.sqlite3 отключено';
  end;
end;

procedure TFormMain.BtnCloseClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFormMain.BtnSaveClick(Sender: TObject);
begin
  try
    SQLQueryTable1.ApplyUpdates;
    SQLTransaction1.CommitRetaining;
    SQLQueryTable1.Refresh;
    StatusBar1.SimpleText := 'Данные по таблице успешно сохранены';
  except
    on E: Exception do
    begin
      MessageDlg('Ошибка при сохранении', 'Возникла какая-то ошибка: ' +
        E.Message, mtError, [mbOK],0
      );
      StatusBar1.SimpleText := 'Ошибка сохранения данных по таблице';
    end;
  end;
end;

procedure TFormMain.BtnAddRecordClick(Sender: TObject);
begin
  DBGrid1.DataSource.DataSet.Append;
end;

procedure TFormMain.BtnDeleteRecordClick(Sender: TObject);
begin
  DBGrid1.DataSource.DataSet.Delete;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  Application.OnException := @MyException;
  RootNode := TreeView1.Items.Add(nil, 'Таблицы');
  DBGrid1.Clear;
end;

procedure TFormMain.FormDestroy(Sender: TObject);
begin
  SQLQueryList.Close;
  SQLQueryTable1.Close;
  SQLTransaction1.Active := False;
  SQLConnector1.Connected := False;
end;

procedure TFormMain.TreeView1SelectionChanged(Sender: TObject);
var
  TmpNode: TTreeNode;
begin
  TmpNode := TreeView1.Selected;

  if (TmpNode = RootNode) or (TmpNode = nil)
  then
  begin
    DBGrid1.Clear;
    SQLQueryTable1.Close;
    BtnAddRecord.Enabled := False;
    BtnDeleteRecord.Enabled := False;
    BtnRefresh.Enabled := False;
    BtnSave.Enabled := False;
    StatusBar1.SimpleText := '';
    Exit;
  end;

  DBGrid1.Clear;
  SQLQueryTable1.Close;

  try
    SQLQueryTable1.SQL.Text := 'SELECT * FROM ' + TmpNode.Text + ';';
    SQLTransaction1.Active := True;
    SQLQueryTable1.Open;

    BtnAddRecord.Enabled := True;
    BtnDeleteRecord.Enabled := True;
    BtnRefresh.Enabled := True;
    BtnSave.Enabled := True;
    StatusBar1.SimpleText := 'Успешная загрузка данных по таблице: ' +
      TmpNode.Text;
  except
    on E: Exception do
    begin
      MessageDlg('Ошибка при загрузке таблицы', 'Возникла какая-то ошибка: ' +
        E.Message,
        mtError, [mbOK],0
      );
      StatusBar1.SimpleText := 'Ошибка загрузки данных по таблице: ' +
        TmpNode.Text;
    end;
  end;
end;

end.

